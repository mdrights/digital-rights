## How to generate these slides

`for f in *.md; do echo ">> $f"; landslide -i -r $f -d ./slides/${f/md/html}; done`

## 如何閱讀這裏的 HTML 幻燈片

將整個倉庫下載下來（zip 形式），或用 Git 客戶端 clone 下來。  
然後用瀏覽器打開即可。（按`T`可查看目錄）  

