# antiS: 不留痕跡的、抗木馬的電腦操作系統

如果你：

- 電腦有可能被警察等暴力機關沒收、取證；或
- 有可能會收到別人發送過來的郵件、文件，其中可能有惡意代碼、木馬  

那麼你需要使用如 antiS 這種 live 類型的操作系統。它能幫你：

- 使用電腦後（重啓後）不留任何痕跡，難以被取證；  
- 可以暫時下載、打開可疑的文件，即使系統中毒、中木馬，也不會有持續的影響。

詳細的介紹請閱讀：[2021-安全培训/6-LiveOS-使用建議](https://gitlab.com/mdrights/digital-rights/-/blob/main/T%E6%95%99%E7%A8%8B%E5%92%8C%E7%AC%94%E8%AE%B0/2021-%E5%AE%89%E5%85%A8%E5%9F%B9%E8%AE%AD/6-LiveOS-%E4%BD%BF%E7%94%A8%E5%BB%BA%E8%AD%B0.md)


最新的系統更新通知，和安裝方法，在[項目代碼庫](https://github.com/mdrights/liveslak)。  

