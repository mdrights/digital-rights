
# Anti-1984，抵抗国家机器对公民的监控

> ——在这个“大數據”和實名制的时代，我能做的，就是尽力提升自己/朋友的安全/隐私意识和能力，并捍卫网络隱私/信息自由。

<hr>

## 这里有：

**数字安全/隐私领域的翻译**：

- 隐匿上网踪迹方法和资源
	- 【推荐】[在线匿名FAQ](https://gitlab.com/atgfw/Digital-rights/blob/main/A%E5%8C%BF%E5%90%8D%E7%AD%96%E7%95%A5/%E5%9C%A8%E7%BA%BF%E5%8C%BF%E5%90%8DFAQ.md) [原创]

- [邮件安全、反钓鱼](https://gitlab.com/atgfw/Digital-rights/tree/main/M%E9%82%AE%E4%BB%B6%E5%AE%89%E5%85%A8)
	- 来自专业安全邮件服务的科普文章。  

- [数据的加密和安全删除](https://gitlab.com/atgfw/Digital-rights/tree/main/e%E5%8A%A0%E5%AF%86%E6%8A%80%E8%A1%93)
	- 把`PGP`等端对端加密用好。
    - [你真的了解端到端加密么？](https://gitlab.com/atgfw/digital-rights/-/blob/main/e%E5%8A%A0%E5%AF%86%E6%8A%80%E8%A1%93/2019-09-21-%E4%BD%A0%E7%9C%9F%E7%9A%84%E4%BA%86%E8%A7%A3%E7%AB%AF%E5%88%B0%E7%AB%AF%E5%8A%A0%E5%AF%86%E4%B9%88.md)

- [數據的安全刪除](https://gitlab.com/atgfw/Digital-rights/blob/master/T%E6%95%99%E7%A8%8B%E5%92%8C%E7%AC%94%E8%AE%B0)
    - [彻底删除数据 on macOS/Linux](https://gitlab.com/atgfw/digital-rights/-/blob/main/R%E6%95%B0%E6%8D%AE%E5%88%A0%E9%99%A4/2021-06-13-%E5%BD%BB%E5%BA%95%E5%88%A0%E9%99%A4-macOS-Linux.md)

- [物理安全](https://gitlab.com/atgfw/Digital-rights/tree/main/W%E7%89%A9%E7%90%86%E5%AE%89%E5%85%A8)
	- [遭遇權力機關的暴力取證？](https://gitlab.com/atgfw/Digital-rights/blob/main/W%E7%89%A9%E7%90%86%E5%AE%89%E5%85%A8/2017-07-30-%E5%A6%82%E6%9E%9C%E4%BD%A0%E7%9A%84%E6%89%8B%E6%9C%BA%E8%A2%AB%E8%AD%A6%E5%AF%9F%E6%B2%A1%E6%94%B6%E4%BD%A0%E8%AF%A5%E6%80%8E%E4%B9%88%E5%8A%9E.md)
	- [过境时保护自己的数据](https://gitlab.com/atgfw/Digital-rights/blob/main/W%E7%89%A9%E7%90%86%E5%AE%89%E5%85%A8/2018-09-20-%E5%A6%82%E4%BD%95%E5%9C%A8%E8%B7%A8%E8%B6%8A%E5%9B%BD%E5%A2%83%E6%97%B6%E4%BF%9D%E6%8A%A4%E4%BD%A0%E7%9A%84%E6%89%8B%E6%9C%BA%E6%88%96%E7%94%B5%E8%84%91.md)

- [數字身份的確認](https://gitlab.com/atgfw/digital-rights/-/blob/main/I%E8%BA%AB%E4%BB%BD%E8%AE%A4%E8%AF%81/2020-03-16-%E5%A6%82%E4%BD%95%E9%AA%8C%E8%AF%81%E4%BB%96%E4%BA%BA%E7%9A%84%E6%95%B0%E5%AD%97%E8%BA%AB%E4%BB%BD-Mailvelope%E7%89%88.md)
    - 如何確保你的隊友是你的隊友？  

- [文件的元信息的處理](https://gitlab.com/atgfw/digital-rights/-/tree/main/Y%E5%AA%92%E4%BD%93%E5%85%83%E4%BF%A1%E6%81%AF)
    - 媒體記者必知，保護自己保護信源  

- [數字安全和隱私保護培训教程](https://gitlab.com/atgfw/digital-rights/-/tree/main/T%E6%95%99%E7%A8%8B%E5%92%8C%E7%AC%94%E8%AE%B0/2021-%E5%AE%89%E5%85%A8%E5%9F%B9%E8%AE%AD)
    - 學習評估你的風險、場景和對手：你的威脅模式
    - “安全的”工具一覽
    - 數據加密和刪除
    - 元信息
    - “Live 操作系統”的使用
    - 更多⋯⋯







<br /> 
<hr>

## 联系？



<br />

## 版权声明

**如无特别说明，这里的译文均采用[CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.zh)协议发布。转载时请保留原文和译文的作者/译者及链接，以及相同的许可协议。**

## Disclaimer

注：这里翻译的商业机构的文章不代表完全同意/支持他们的服务；翻译作品如无特定声明则均未得到作者的认可。

<br />
<hr>

## Warrant Canary

这是[「金丝雀」](https://en.wikipedia.org/wiki/Warrant_canary)，如果我飞走了，请不要联系我所有以`atgfw`为名的帐号；没飞走，说明我还安全。  
兄弟爬山，各自找路。万事小心。

Updated: 2021.06.01
